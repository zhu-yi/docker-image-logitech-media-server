#!/bin/sh

# SPDX-FileCopyrightText: 2020 Mark Jonas <toertel@gmail.com>
# SPDX-License-Identifier: MIT

if [ $# -ne 1 ]; then
  echo "Usage: $0 IMAGE" >&2
  exit 1
fi

IMAGE=$1
CONTAINER="lms-test"
TESTDIR=$(mktemp -d)
TESTFILE=$(mktemp)

setup() {
    echo "Setting up test environment ..."

    mkdir "${TESTDIR}/squeezebox"
    mkdir "${TESTDIR}/music"

    if [ "$(docker ps -q -f name=${CONTAINER})" ]; then
        echo "ERROR: Container ${CONTAINER} is already running." >&2
        exit 1
    else
        if [ "$(docker ps -aq -f status=exited -f name=${CONTAINER})" ]; then
            echo "WARNING: Removing container ${CONTAINER}." >&2
            docker rm ${CONTAINER}
        fi
    fi

    docker run -d --init \
        -p 9000:9000 -p 9090:9090 -p 3483:3483 -p 3483:3483/udp \
        -v /dev/null:/etc/localtime:ro \
        -v "${TESTDIR}/squeezebox":/srv/squeezebox \
        -v "${TESTDIR}/music":/srv/music \
        --name "${CONTAINER}" \
        "${IMAGE}"
    test_eval $? "Started container ${CONTAINER}."

    sleep 1

    if [ "$(docker ps -q -f name=${CONTAINER})" ]; then
        result=0
    else
        result=1
    fi
    test_eval $result "Container ${CONTAINER} is running."

    docker exec -t ${CONTAINER} apt-get update -y -qq
    test_eval $? "Update package db."

    docker exec -t ${CONTAINER} apt-get install -y -qq netcat procps
    test_eval $? "Install additional packages."
}

teardown() {
    echo "Tearing down test environment ..."

    docker stop "${CONTAINER}"
    docker rm "${CONTAINER}"

    rm -rf "${TESTDIR}"
    rm "${TESTFILE}"
}

test_processes() {
    docker exec -t ${CONTAINER} ps -A -o euser:16= -o comm:16= | tee "${TESTFILE}"
    grep -q "squeezeboxserver squeezeboxserve" < "${TESTFILE}"
    test_eval $? "squeezeboxserver running with effective user squeezeboxserver."
}

test_ports() {
    tcp_ports="9000 9090 3483"
    for p in $tcp_ports; do
        docker exec -t ${CONTAINER} nc -zv localhost "$p"
        test_eval $? "TCP port $p connectable."
    done

    docker exec -t ${CONTAINER} nc -zvu localhost 3483
    test_eval $? "UDP port 3483 connectable."
}

test_gui() {
    docker exec -t ${CONTAINER} curl -s http://localhost:9000 | grep -q "<title>Logitech Media Server</title>"
    test_eval $? "Logitech Media Server web UI is up."
}

test_eval() {
    if [ "$1" -ne 0 ]; then
        echo "FAIL: $2"
        exit 1;
    else
        echo "PASS: $2"
    fi
}

trap teardown EXIT
setup
test_processes
test_ports
test_gui
