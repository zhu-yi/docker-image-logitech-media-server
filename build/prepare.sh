#!/bin/sh

# SPDX-FileCopyrightText: 2020 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

QEMU_VERSION=v3.1.0-2
QEMU_ARCHS="x86_64 arm aarch64"

if [ $# -eq 1 ]; then
  archs="$1"
else
  archs="${QEMU_ARCHS}"
fi

# Exit immediately if a command exits with a non-zero status
set -e

TMPDIR=$(mktemp -d -t qemu-static_XXXXXX)

cleanup () {
    rm -rf "${TMPDIR}"
}

trap cleanup EXIT

for target_arch in ${archs}; do
  wget -N -P "${TMPDIR}" "https://github.com/multiarch/qemu-user-static/releases/download/${QEMU_VERSION}/x86_64_qemu-${target_arch}-static.tar.gz"
  tar -xvf "${TMPDIR}/x86_64_qemu-${target_arch}-static.tar.gz"
done
