#!/bin/sh

# SPDX-FileCopyrightText: 2020 Mark Jonas <toertel@gmail.com>
#
# SPDX-License-Identifier: MIT

# Exit immediately if a command exits with a non-zero status
set -e

if [ "$#" -ne 3 ]; then
  echo "Usage: $0 REGISTRY_USER REGISTRY_REPO ARCHITECTURE" >&2
  exit 1
fi

CONTAINER_REGISTRY_USER="$1"
CONTAINER_REGISTRY_REPO="$2"
CONTAINER_ARCH="$3"

container_tag="${CONTAINER_REGISTRY_USER}/${CONTAINER_REGISTRY_REPO}:${CONTAINER_ARCH}-latest"
echo "+++ Build ${container_tag}"
docker build -f "Dockerfile.${CONTAINER_ARCH}" -t "${container_tag}" .
