#!/bin/sh

# SPDX-FileCopyrightText: 2015-2017 Lars Kellogg-Stedman <lars@oddbit.com>
# SPDX-FileCopyrightText: 2017 Richard van den Berg <richard@vdberg.org>
#
# SPDX-License-Identifier: MIT

myip=$( ip addr show eth0 2> /dev/null | awk '$1 == "inet" {print $2}' | cut -f1 -d/ )

if [ "$myip" ]; then
	url="http://$myip:9000/"

	echo ======================================================================
	echo "$url"
	echo ======================================================================
	echo
fi

exec squeezeboxserver \
	--prefsdir "$SQUEEZE_VOL/prefs" \
	--logdir "$SQUEEZE_VOL/logs" \
	--cachedir "$SQUEEZE_VOL/cache" "$@"
